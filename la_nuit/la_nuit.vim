" Generated with help from http://bytefluent.com/vivify 2022-11-02
set background=dark
if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = "la nuit dans le 17éme"

"hi CTagsMember -- no settings --
"hi CTagsGlobalConstant -- no settings --
"hi Ignore -- no settings --
hi Normal guifg=#c2c2c2 guibg=#1e1f21 guisp=#191a1c gui=NONE ctermfg=7 ctermbg=234 cterm=NONE
"hi CTagsImport -- no settings --
"hi CTagsGlobalVariable -- no settings --
"hi EnumerationValue -- no settings --
"hi Union -- no settings --
"hi Question -- no settings --
"hi EnumerationName -- no settings --
"hi DefinedName -- no settings --
"hi LocalVariable -- no settings --
"hi CTagsClass -- no settings --
"hi clear -- no settings --
hi IncSearch guifg=#191a1c guibg=#f7d55b guisp=#f7d55b gui=NONE ctermfg=234 ctermbg=221 cterm=NONE
hi WildMenu guifg=NONE guibg=#A1A6A8 guisp=#A1A6A8 gui=NONE ctermfg=NONE ctermbg=248 cterm=NONE
hi SignColumn guifg=#192224 guibg=NONE guisp=#536991 gui=NONE ctermfg=235 ctermbg=NONE cterm=NONE
hi SpecialComment guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Typedef guifg=#536991 guibg=NONE guisp=NONE gui=italic ctermfg=60 ctermbg=NONE cterm=italic
hi Title guifg=#e2e4e5 guibg=#484b6e guisp=#192224 gui=italic ctermfg=189 ctermbg=235 cterm=italic
hi Folded guifg=#192224 guibg=#A1A6A8 guisp=#A1A6A8 gui=italic ctermfg=235 ctermbg=248 cterm=NONE
hi PreCondit guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Include guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi TabLineSel guifg=#192224 guibg=#bd9700 guisp=#bd9700 gui=italic ctermfg=235 ctermbg=1 cterm=italic
hi StatusLineNC guifg=#191a1c guibg=#687fad guisp=#687fad gui=italic ctermfg=234 ctermbg=67 cterm=italic
hi NonText guifg=#191a1c guibg=#1e1f21 guisp=#191a1c gui=italic ctermfg=234 ctermbg=234 cterm=NONE
hi DiffText guifg=#080914 guibg=#bf6054 guisp=#bf6054 gui=NONE ctermfg=233 ctermbg=167 cterm=NONE
hi ErrorMsg guifg=#080914 guibg=#bf6054 guisp=#bf6054 gui=NONE ctermfg=233 ctermbg=167 cterm=NONE
hi Debug guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi PMenuSbar guifg=#e2e4e5 guibg=#687fad guisp=#687fad gui=NONE ctermfg=254 ctermbg=67 cterm=NONE
hi Identifier guifg=#c2c2c2 guibg=NONE guisp=NONE gui=italic ctermfg=7 ctermbg=NONE cterm=NONE
hi SpecialChar guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Conditional guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi StorageClass guifg=#536991 guibg=NONE guisp=NONE gui=italic ctermfg=60 ctermbg=NONE cterm=italic
hi Todo guifg=#09050a guibg=#6cacb4 guisp=#6cacb4 gui=italic,underline ctermfg=232 ctermbg=73 cterm=italic,underline
hi Special guifg=#c49d02 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=NONE
hi LineNr guifg=#626770 guibg=#191a1c guisp=#191a1c gui=NONE ctermfg=242 ctermbg=234 cterm=NONE
hi StatusLine guifg=#192224 guibg=#785335 guisp=#785335 gui=italic ctermfg=235 ctermbg=95 cterm=italic
hi Label guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi PMenuSel guifg=#192224 guibg=#bd9700 guisp=#bd9700 gui=italic ctermfg=235 ctermbg=1 cterm=italic
hi Search guifg=#191a1c guibg=#f7d55b guisp=#f7d55b gui=NONE ctermfg=234 ctermbg=221 cterm=NONE
hi Delimiter guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Statement guifg=#ffe3cc guibg=NONE guisp=NONE gui=italic ctermfg=224 ctermbg=NONE cterm=italic
hi Comment guifg=#191a1c guibg=#916e83 guisp=#916e83 gui=italic ctermfg=234 ctermbg=96 cterm=NONE
hi Character guifg=#A1A6A8 guibg=NONE guisp=NONE gui=NONE ctermfg=248 ctermbg=NONE cterm=NONE
hi Float guifg=#A1A6A8 guibg=NONE guisp=NONE gui=NONE ctermfg=248 ctermbg=NONE cterm=NONE
hi NormalFloat guibg=#191d38 ctermbg=NONE
hi Number guifg=#080914 guibg=#687fad guisp=#687fad gui=NONE ctermfg=233 ctermbg=67 cterm=NONE
hi Boolean guifg=#A1A6A8 guibg=NONE guisp=NONE gui=NONE ctermfg=248 ctermbg=NONE cterm=NONE
hi Operator guifg=#bd9700 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi CursorLine guifg=NONE guibg=#222E30 guisp=#222E30 gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorLineNr guifg=#b48ca4 guibg=nonw ctermfg=7 ctermbg=NONE cterm=NONE
hi TabLineFill guifg=#ffe3cc guibg=#191a1c guisp=#191a1c gui=italic ctermfg=224 ctermbg=234 cterm=italic
hi WarningMsg guifg=#A1A6A8 guibg=#912C00 guisp=#912C00 gui=NONE ctermfg=248 ctermbg=88 cterm=NONE
hi VisualNOS guifg=#192224 guibg=#F9F9FF guisp=#F9F9FF gui=underline ctermfg=235 ctermbg=189 cterm=underline
hi DiffDelete guifg=#080914 guibg=#db3939 guisp=#192224 gui=NONE ctermfg=NONE ctermbg=235 cterm=NONE
hi ModeMsg guifg=#827f82 guibg=#191a1c guisp=#191a1c gui=italic ctermfg=8 ctermbg=234 cterm=italic
hi CursorColumn guifg=NONE guibg=#222E30 guisp=#222E30 gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi Define guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Function guifg=#5f77a1 guibg=NONE guisp=NONE gui=italic ctermfg=67 ctermbg=NONE cterm=italic
hi FoldColumn guifg=#192224 guibg=#A1A6A8 guisp=#A1A6A8 gui=italic ctermfg=235 ctermbg=248 cterm=NONE
hi PreProc guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Visual guifg=#192224 guibg=#F9F9FF guisp=#F9F9FF gui=NONE ctermfg=235 ctermbg=189 cterm=NONE
hi MoreMsg guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi VertSplit guifg=#192224 guibg=#5E6C70 guisp=#5E6C70 gui=italic ctermfg=235 ctermbg=66 cterm=italic
hi Exception guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi Keyword guifg=#bd9700 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi Type guifg=#7492c2 guibg=NONE guisp=NONE gui=italic ctermfg=110 ctermbg=NONE cterm=italic
hi DiffChange guifg=#e2e4e5 guibg=#484b6e guisp=#484b6e gui=NONE ctermfg=254 ctermbg=60 cterm=NONE
hi Cursor guifg=#191a1c guibg=#c2c2c2 guisp=#c2c2c2 gui=NONE ctermfg=234 ctermbg=7 cterm=NONE
hi Error guifg=#A1A6A8 guibg=#912C00 guisp=#912C00 gui=NONE ctermfg=248 ctermbg=88 cterm=NONE
hi PMenu guifg=#192224 guibg=#687fad guisp=#687fad gui=italic ctermfg=235 ctermbg=67 cterm=NONE
hi SpecialKey guifg=#5E6C70 guibg=NONE guisp=NONE gui=italic ctermfg=66 ctermbg=NONE cterm=NONE
hi Constant guifg=#080914 guibg=#687fad guisp=#687fad gui=NONE ctermfg=233 ctermbg=67 cterm=NONE
hi Tag guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi String guifg=#080914 guibg=#687fad guisp=#687fad gui=italic ctermfg=233 ctermbg=67 cterm=NONE
hi PMenuThumb guifg=#e2e4e5 guibg=#a4a6a8 guisp=#a4a6a8 gui=NONE ctermfg=254 ctermbg=248 cterm=NONE
hi MatchParen guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
hi Repeat guifg=#BD9800 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=italic
" hi SpellBad guifg=#F9F9FF guibg=#192224 guisp=#192224 gui=underline ctermfg=189 ctermbg=235 cterm=underline
" hi SpellLocal guifg=#F9F9FF guibg=#192224 guisp=#192224 gui=underline ctermfg=189 ctermbg=235 cterm=underline
" hi SpellCap guifg=#F9F9FF guibg=#192224 guisp=#192224 gui=underline ctermfg=189 ctermbg=235 cterm=underline
" hi SpellRare guifg=#F9F9FF guibg=#192224 guisp=#192224 gui=underline ctermfg=189 ctermbg=235 cterm=underline
hi SpellLocal gui=undercurl guisp=lightgrey cterm=underline ctermfg=red
hi SpellCap gui=undercurl guisp=lightblue cterm=underline ctermfg=red
hi SpellRare gui=undercurl guisp=orange cterm=underline ctermfg=red
hi SpellBad gui=undercurl guisp=red cterm=underline ctermfg=red
hi Directory guifg=#536991 guibg=NONE guisp=NONE gui=italic ctermfg=60 ctermbg=NONE cterm=italic
hi Structure guifg=#536991 guibg=NONE guisp=NONE gui=italic ctermfg=60 ctermbg=NONE cterm=italic
hi Macro guifg=#BD9800 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Underlined guifg=#F9F9FF guibg=#192224 guisp=#192224 gui=underline ctermfg=189 ctermbg=235 cterm=underline
hi DiffAdd guifg=#080914 guibg=#83b598 guisp=#83b598 gui=NONE ctermfg=233 ctermbg=108 cterm=NONE
hi TabLine guifg=#191a1c guibg=#687fad guisp=#687fad gui=italic ctermfg=234 ctermbg=67 cterm=italic
hi cursorim guifg=#192224 guibg=#536991 guisp=#536991 gui=NONE ctermfg=235 ctermbg=60 cterm=NONE
