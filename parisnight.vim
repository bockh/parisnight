" Copyright (C) 2022-present Jacob Field <jacob.field@gmail.com>

" Project: Paris Night
" Repository: 
" License: MIT
" based on Nord

if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif

let g:colors_name = "paris"
let s:paris_vim_version="0.18.0"
set background=dark

let s:paris0_gui = "#2E3440"
let s:paris1_gui = "#3B4252"
let s:paris2_gui = "#434C5E"
let s:paris3_gui = "#4C566A"
let s:paris3_gui_bright = "#616E88"
let s:paris4_gui = "#D8DEE9"
let s:paris5_gui = "#E5E9F0"
let s:paris6_gui = "#ECEFF4"
let s:paris7_gui = "#8FBCBB"
let s:paris8_gui = "#88C0D0"
let s:paris9_gui = "#81A1C1"
let s:paris10_gui = "#5E81AC"
let s:paris11_gui = "#BF616A"
let s:paris12_gui = "#D08770"
let s:paris13_gui = "#EBCB8B"
let s:paris14_gui = "#A3BE8C"
let s:paris15_gui = "#B48EAD"

" let s:paris1_term = "0" black
" let s:paris3_term = "8" gray
" let s:paris5_term = "7" silver (lighter gray)
" let s:paris6_term = "15" white
" let s:paris7_term = "14" aqua
" let s:paris8_term = "6" teal
" let s:paris9_term = "4" navy
" let s:paris10_term = "12" blue
" let s:paris11_term = "1" maroon
" let s:paris12_term = "11" yellow
" let s:paris13_term = "3" olive
" let s:paris14_term = "2" green
" let s:paris15_term = "5" purple

" highlight col and row
let s:paris1_term = "238"
" let s:paris1_term = "60"

" row numbers and comments
let s:paris3_term = "60"

let s:paris5_term = "2"
" brackets
let s:paris6_term = "7"
 " keywords function, var
let s:paris7_term = "142"

let s:paris8_term = "178"

" keywords let
let s:paris9_term = "136"

let s:paris10_term = "22"
" misspellings
let s:paris11_term = "1"

let s:paris12_term = "11"

let s:paris13_term = "1"
" strings
let s:paris14_term = "103"
" args
let s:paris15_term = "3"

let s:paris3_gui_brightened = [
  \ s:paris3_gui,
  \ "#4e586d",
  \ "#505b70",
  \ "#525d73",
  \ "#556076",
  \ "#576279",
  \ "#59647c",
  \ "#5b677f",
  \ "#5d6982",
  \ "#5f6c85",
  \ "#616e88",
  \ "#63718b",
  \ "#66738e",
  \ "#687591",
  \ "#6a7894",
  \ "#6d7a96",
  \ "#6f7d98",
  \ "#72809a",
  \ "#75829c",
  \ "#78859e",
  \ "#7b88a1",
\ ]

let g:paris_bold = get(g:, "paris_bold", 1)
let s:bold = (g:paris_bold == 0) ? "" : "bold,"

let g:paris_underline = get(g:, "paris_underline", 1)
let s:underline = (g:paris_underline == 0) ? "NONE," : "underline,"

let g:paris_italic = get(g:, "paris_italic", (has("gui_running") || $TERM_ITALICS == "true"))
let s:italic = (g:paris_italic == 0) ? "" : "italic,"

let g:paris_italic_comments = get(g:, "paris_italic_comments", 0)
let s:italicize_comments = (g:paris_italic_comments == 0) ? "" : get(s:, "italic")

let g:paris_uniform_status_lines = get(g:, "paris_uniform_status_lines", 0)

let g:paris_bold_vertical_split_line = get(g:, "paris_bold_vertical_split_line", 0)
if exists("g:paris_comment_brightness")
  echohl WarningMsg
  echomsg 'paris: warning: Variable g:paris_comment_brightness has been deprecated and will be removed in version 1.0.0!' .
                   \' The comment color brightness has been increased by 10% by default.' .
                   \' Please see https://github.com/arcticicestudio/paris-vim/issues/145 for more details.'
  echohl None
  let g:paris_comment_brightness = 10
endif
let g:paris_cursor_line_number_background = get(g:, "paris_cursor_line_number_background", 0)
let g:paris_uniform_diff_background = get(g:, "paris_uniform_diff_background", 0)

function! s:hi(group, guifg, guibg, ctermfg, ctermbg, attr, guisp)
  let cmd = ""
  if a:guifg != ""
    let cmd = cmd . " guifg=" . a:guifg
  endif
  if a:guibg != ""
    let cmd = cmd . " guibg=" . a:guibg
  endif
  if a:ctermfg != ""
    let cmd = cmd . " ctermfg=" . a:ctermfg
  endif
  if a:ctermbg != ""
    let cmd = cmd . " ctermbg=" . a:ctermbg
  endif
  if a:attr != ""
    let cmd = cmd . " gui=" . a:attr . " cterm=" . substitute(a:attr, "undercurl", s:underline, "")
  endif
  if a:guisp != ""
    let cmd = cmd . " guisp=" . a:guisp
  endif
  if cmd != ""
    exec "hi " . a:group . cmd
  endif
endfunction

"+---------------+
"+ UI Components +
"+---------------+
"+--- Attributes ---+
call s:hi("Bold", "", "", "", "", s:bold, "")
call s:hi("Italic", "", "", "", "", s:italic, "")
call s:hi("Underline", "", "", "", "", s:underline, "")

"+--- Editor ---+
call s:hi("ColorColumn", "", s:paris1_gui, "NONE", s:paris1_term, "", "")
call s:hi("Cursor", s:paris0_gui, s:paris4_gui, "", "NONE", "", "")
call s:hi("CursorLine", "", s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
call s:hi("Error", s:paris4_gui, s:paris11_gui, "", s:paris11_term, "", "")
call s:hi("iCursor", s:paris0_gui, s:paris4_gui, "", "NONE", "", "")
call s:hi("LineNr", s:paris3_gui, "NONE", s:paris3_term, "NONE", "", "")
call s:hi("MatchParen", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "", "")
call s:hi("NonText", s:paris2_gui, "", s:paris3_term, "", "", "")
call s:hi("Normal", s:paris4_gui, s:paris0_gui, "NONE", "NONE", "", "")
call s:hi("Pmenu", s:paris4_gui, s:paris2_gui, "NONE", s:paris1_term, "NONE", "")
call s:hi("PmenuSbar", s:paris4_gui, s:paris2_gui, "NONE", s:paris1_term, "", "")
call s:hi("PmenuSel", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "", "")
call s:hi("PmenuThumb", s:paris8_gui, s:paris3_gui, "NONE", s:paris3_term, "", "")
call s:hi("SpecialKey", s:paris3_gui, "", s:paris3_term, "", "", "")
call s:hi("SpellBad", s:paris11_gui, s:paris0_gui, s:paris11_term, "NONE", "undercurl", s:paris11_gui)
call s:hi("SpellCap", s:paris13_gui, s:paris0_gui, s:paris13_term, "NONE", "undercurl", s:paris13_gui)
call s:hi("SpellLocal", s:paris5_gui, s:paris0_gui, s:paris5_term, "NONE", "undercurl", s:paris5_gui)
call s:hi("SpellRare", s:paris6_gui, s:paris0_gui, s:paris6_term, "NONE", "undercurl", s:paris6_gui)
call s:hi("Visual", "", s:paris2_gui, "", s:paris1_term, "", "")
call s:hi("VisualNOS", "", s:paris2_gui, "", s:paris1_term, "", "")

"+- Vim 8 Terminal Colors -+
if has('terminal')
  let g:terminal_ansi_colors = [s:paris1_gui, s:paris11_gui, s:paris14_gui, s:paris13_gui, s:paris9_gui, s:paris15_gui, s:paris8_gui, s:paris5_gui, s:paris3_gui, s:paris11_gui, s:paris14_gui, s:paris13_gui, s:paris9_gui, s:paris15_gui, s:paris7_gui, s:paris6_gui]
endif

if has('nvim')
  "+- Neovim Terminal Colors -+
  let g:terminal_color_0 = s:paris1_gui
  let g:terminal_color_1 = s:paris11_gui
  let g:terminal_color_2 = s:paris14_gui
  let g:terminal_color_3 = s:paris13_gui
  let g:terminal_color_4 = s:paris9_gui
  let g:terminal_color_5 = s:paris15_gui
  let g:terminal_color_6 = s:paris8_gui
  let g:terminal_color_7 = s:paris5_gui
  let g:terminal_color_8 = s:paris3_gui
  let g:terminal_color_9 = s:paris11_gui
  let g:terminal_color_10 = s:paris14_gui
  let g:terminal_color_11 = s:paris13_gui
  let g:terminal_color_12 = s:paris9_gui
  let g:terminal_color_13 = s:paris15_gui
  let g:terminal_color_14 = s:paris7_gui
  let g:terminal_color_15 = s:paris6_gui

  "+- Neovim Support -+
  call s:hi("healthError", s:paris11_gui, s:paris1_gui, s:paris11_term, s:paris1_term, "", "")
  call s:hi("healthSuccess", s:paris14_gui, s:paris1_gui, s:paris14_term, s:paris1_term, "", "")
  call s:hi("healthWarning", s:paris13_gui, s:paris1_gui, s:paris13_term, s:paris1_term, "", "")
  call s:hi("TermCursorNC", "", s:paris1_gui, "", s:paris1_term, "", "")

  "+- Neovim Diagnostics API -+
  call s:hi("DiagnosticWarn", s:paris13_gui, "", s:paris13_term, "", "", "")
  call s:hi("DiagnosticError" , s:paris11_gui, "", s:paris11_term, "", "", "")
  call s:hi("DiagnosticInfo" , s:paris8_gui, "", s:paris8_term, "", "", "")
  call s:hi("DiagnosticHint" , s:paris10_gui, "", s:paris10_term, "", "", "")
  call s:hi("DiagnosticUnderlineWarn" , s:paris13_gui, "", s:paris13_term, "", "undercurl", "")
  call s:hi("DiagnosticUnderlineError" , s:paris11_gui, "", s:paris11_term, "", "undercurl", "")
  call s:hi("DiagnosticUnderlineInfo" , s:paris8_gui, "", s:paris8_term, "", "undercurl", "")
  call s:hi("DiagnosticUnderlineHint" , s:paris10_gui, "", s:paris10_term, "", "undercurl", "")

  "+- Neovim DocumentHighlight -+
  call s:hi("LspReferenceText", "", s:paris3_gui, "", s:paris3_term, "", "")
  call s:hi("LspReferenceRead", "", s:paris3_gui, "", s:paris3_term, "", "")
  call s:hi("LspReferenceWrite", "", s:paris3_gui, "", s:paris3_term, "", "")

  "+- Neovim LspSignatureHelp -+
  call s:hi("LspSignatureActiveParameter", s:paris8_gui, "", s:paris8_term, "", s:underline, "")
endif

"+--- Gutter ---+
call s:hi("CursorColumn", "", s:paris1_gui, "NONE", s:paris1_term, "", "")
if g:paris_cursor_line_number_background == 0
  call s:hi("CursorLineNr", s:paris4_gui, "", "NONE", "", "NONE", "")
else
  call s:hi("CursorLineNr", s:paris4_gui, s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
endif
call s:hi("Folded", s:paris3_gui, s:paris1_gui, s:paris3_term, s:paris1_term, s:bold, "")
call s:hi("FoldColumn", s:paris3_gui, s:paris0_gui, s:paris3_term, "NONE", "", "")
call s:hi("SignColumn", s:paris1_gui, s:paris0_gui, s:paris1_term, "NONE", "", "")

"+--- Navigation ---+
call s:hi("Directory", s:paris8_gui, "", s:paris8_term, "NONE", "", "")

"+--- Prompt/Status ---+
call s:hi("EndOfBuffer", s:paris1_gui, "", s:paris1_term, "NONE", "", "")
call s:hi("ErrorMsg", s:paris4_gui, s:paris11_gui, "NONE", s:paris11_term, "", "")
call s:hi("ModeMsg", s:paris4_gui, "", "", "", "", "")
call s:hi("MoreMsg", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("Question", s:paris4_gui, "", "NONE", "", "", "")
if g:paris_uniform_status_lines == 0
  call s:hi("StatusLine", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "NONE", "")
  call s:hi("StatusLineNC", s:paris4_gui, s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
  call s:hi("StatusLineTerm", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:paris4_gui, s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
else
  call s:hi("StatusLine", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "NONE", "")
  call s:hi("StatusLineNC", s:paris4_gui, s:paris3_gui, "NONE", s:paris3_term, "NONE", "")
  call s:hi("StatusLineTerm", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:paris4_gui, s:paris3_gui, "NONE", s:paris3_term, "NONE", "")
endif
call s:hi("WarningMsg", s:paris0_gui, s:paris13_gui, s:paris1_term, s:paris13_term, "", "")
call s:hi("WildMenu", s:paris8_gui, s:paris1_gui, s:paris8_term, s:paris1_term, "", "")

"+--- Search ---+
call s:hi("IncSearch", s:paris6_gui, s:paris10_gui, s:paris6_term, s:paris10_term, s:underline, "")
call s:hi("Search", s:paris1_gui, s:paris8_gui, s:paris1_term, s:paris8_term, "NONE", "")

"+--- Tabs ---+
call s:hi("TabLine", s:paris4_gui, s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
call s:hi("TabLineFill", s:paris4_gui, s:paris1_gui, "NONE", s:paris1_term, "NONE", "")
call s:hi("TabLineSel", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, "NONE", "")

"+--- Window ---+
call s:hi("Title", s:paris4_gui, "", "NONE", "", "NONE", "")

if g:paris_bold_vertical_split_line == 0
  call s:hi("VertSplit", s:paris2_gui, s:paris0_gui, s:paris3_term, "NONE", "NONE", "")
else
  call s:hi("VertSplit", s:paris2_gui, s:paris1_gui, s:paris3_term, s:paris1_term, "NONE", "")
endif

"+----------------------+
"+ Language Base Groups +
"+----------------------+
call s:hi("Boolean", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Character", s:paris14_gui, "", s:paris14_term, "", "", "")
call s:hi("Comment", s:paris3_gui_bright, "", s:paris3_term, "", s:italicize_comments, "")
call s:hi("Conceal", "", "NONE", "", "NONE", "", "")
call s:hi("Conditional", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Constant", s:paris4_gui, "", "NONE", "", "", "")
call s:hi("Decorator", s:paris12_gui, "", s:paris12_term, "", "", "")
call s:hi("Define", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Delimiter", s:paris6_gui, "", s:paris6_term, "", "", "")
call s:hi("Exception", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Float", s:paris15_gui, "", s:paris15_term, "", "", "")
call s:hi("Function", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("Identifier", s:paris4_gui, "", "NONE", "", "NONE", "")
call s:hi("Include", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Keyword", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Label", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Number", s:paris15_gui, "", s:paris15_term, "", "", "")
call s:hi("Operator", s:paris9_gui, "", s:paris9_term, "", "NONE", "")
call s:hi("PreProc", s:paris9_gui, "", s:paris9_term, "", "NONE", "")
call s:hi("Repeat", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Special", s:paris4_gui, "", "NONE", "", "", "")
call s:hi("SpecialChar", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("SpecialComment", s:paris8_gui, "", s:paris8_term, "", s:italicize_comments, "")
call s:hi("Statement", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("StorageClass", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("String", s:paris14_gui, "", s:paris14_term, "", "", "")
call s:hi("Structure", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("Tag", s:paris4_gui, "", "", "", "", "")
call s:hi("Todo", s:paris13_gui, "NONE", s:paris13_term, "NONE", "", "")
call s:hi("Type", s:paris9_gui, "", s:paris9_term, "", "NONE", "")
call s:hi("Typedef", s:paris9_gui, "", s:paris9_term, "", "", "")
hi! link Annotation Decorator
hi! link Macro Define
hi! link PreCondit PreProc
hi! link Variable Identifier

"+-----------+
"+ Languages +
"+-----------+
call s:hi("asciidocAttributeEntry", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("asciidocAttributeList", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("asciidocAttributeRef", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("asciidocHLabel", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("asciidocListingBlock", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("asciidocMacroAttributes", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("asciidocOneLineTitle", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("asciidocPassthroughBlock", s:paris9_gui, "", s:paris9_term, "", "", "")
call s:hi("asciidocQuotedMonospaced", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("asciidocTriplePlusPassthrough", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link asciidocAdmonition Keyword
hi! link asciidocAttributeRef markdownH1
hi! link asciidocBackslash Keyword
hi! link asciidocMacro Keyword
hi! link asciidocQuotedBold Bold
hi! link asciidocQuotedEmphasized Italic
hi! link asciidocQuotedMonospaced2 asciidocQuotedMonospaced
hi! link asciidocQuotedUnconstrainedBold asciidocQuotedBold
hi! link asciidocQuotedUnconstrainedEmphasized asciidocQuotedEmphasized
hi! link asciidocURL markdownLinkText

call s:hi("awkCharClass", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("awkPatterns", s:paris9_gui, "", s:paris9_term, "", s:bold, "")
hi! link awkArrayElement Identifier
hi! link awkBoolLogic Keyword
hi! link awkBrktRegExp SpecialChar
hi! link awkComma Delimiter
hi! link awkExpression Keyword
hi! link awkFieldVars Identifier
hi! link awkLineSkip Keyword
hi! link awkOperator Operator
hi! link awkRegExp SpecialChar
hi! link awkSearch Keyword
hi! link awkSemicolon Delimiter
hi! link awkSpecialCharacter SpecialChar
hi! link awkSpecialPrintf SpecialChar
hi! link awkVariables Identifier

call s:hi("cIncluded", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link cOperator Operator
hi! link cPreCondit PreCondit
hi! link cConstant Type

call s:hi("cmakeGeneratorExpression", s:paris10_gui, "", s:paris10_term, "", "", "")

hi! link csPreCondit PreCondit
hi! link csType Type
hi! link csXmlTag SpecialComment

call s:hi("cssAttributeSelector", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("cssDefinition", s:paris7_gui, "", s:paris7_term, "", "NONE", "")
call s:hi("cssIdentifier", s:paris7_gui, "", s:paris7_term, "", s:underline, "")
call s:hi("cssStringQ", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link cssAttr Keyword
hi! link cssBraces Delimiter
hi! link cssClassName cssDefinition
hi! link cssColor Number
hi! link cssProp cssDefinition
hi! link cssPseudoClass cssDefinition
hi! link cssPseudoClassId cssPseudoClass
hi! link cssVendor Keyword

call s:hi("dosiniHeader", s:paris8_gui, "", s:paris8_term, "", "", "")
hi! link dosiniLabel Type

call s:hi("dtBooleanKey", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("dtExecKey", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("dtLocaleKey", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("dtNumericKey", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("dtTypeKey", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link dtDelim Delimiter
hi! link dtLocaleValue Keyword
hi! link dtTypeValue Keyword

if g:paris_uniform_diff_background == 0
  call s:hi("DiffAdd", s:paris14_gui, s:paris0_gui, s:paris14_term, "NONE", "inverse", "")
  call s:hi("DiffChange", s:paris13_gui, s:paris0_gui, s:paris13_term, "NONE", "inverse", "")
  call s:hi("DiffDelete", s:paris11_gui, s:paris0_gui, s:paris11_term, "NONE", "inverse", "")
  call s:hi("DiffText", s:paris9_gui, s:paris0_gui, s:paris9_term, "NONE", "inverse", "")
else
  call s:hi("DiffAdd", s:paris14_gui, s:paris1_gui, s:paris14_term, s:paris1_term, "", "")
  call s:hi("DiffChange", s:paris13_gui, s:paris1_gui, s:paris13_term, s:paris1_term, "", "")
  call s:hi("DiffDelete", s:paris11_gui, s:paris1_gui, s:paris11_term, s:paris1_term, "", "")
  call s:hi("DiffText", s:paris9_gui, s:paris1_gui, s:paris9_term, s:paris1_term, "", "")
endif
" Legacy groups for official git.vim and diff.vim syntax
hi! link diffAdded DiffAdd
hi! link diffChanged DiffChange
hi! link diffRemoved DiffDelete

call s:hi("gitconfigVariable", s:paris7_gui, "", s:paris7_term, "", "", "")

call s:hi("goBuiltins", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link goConstants Keyword

call s:hi("helpBar", s:paris3_gui, "", s:paris3_term, "", "", "")
call s:hi("helpHyperTextJump", s:paris8_gui, "", s:paris8_term, "", s:underline, "")

call s:hi("htmlArg", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("htmlLink", s:paris4_gui, "", "", "", "NONE", "NONE")
hi! link htmlBold Bold
hi! link htmlEndTag htmlTag
hi! link htmlItalic Italic
hi! link htmlH1 markdownH1
hi! link htmlH2 markdownH1
hi! link htmlH3 markdownH1
hi! link htmlH4 markdownH1
hi! link htmlH5 markdownH1
hi! link htmlH6 markdownH1
hi! link htmlSpecialChar SpecialChar
hi! link htmlTag Keyword
hi! link htmlTagN htmlTag

call s:hi("javaDocTags", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link javaCommentTitle Comment
hi! link javaScriptBraces Delimiter
hi! link javaScriptIdentifier Keyword
hi! link javaScriptNumber Number

call s:hi("jsonKeyword", s:paris7_gui, "", s:paris7_term, "", "", "")

call s:hi("lessClass", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link lessAmpersand Keyword
hi! link lessCssAttribute Delimiter
hi! link lessFunction Function
hi! link cssSelectorOp Keyword

hi! link lispAtomBarSymbol SpecialChar
hi! link lispAtomList SpecialChar
hi! link lispAtomMark Keyword
hi! link lispBarSymbol SpecialChar
hi! link lispFunc Function

hi! link luaFunc Function

call s:hi("markdownBlockquote", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownCode", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownCodeDelimiter", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownFootnote", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownId", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownIdDeclaration", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("markdownH1", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("markdownLinkText", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("markdownUrl", s:paris4_gui, "", "NONE", "", "NONE", "")
hi! link markdownBold Bold
hi! link markdownBoldDelimiter Keyword
hi! link markdownFootnoteDefinition markdownFootnote
hi! link markdownH2 markdownH1
hi! link markdownH3 markdownH1
hi! link markdownH4 markdownH1
hi! link markdownH5 markdownH1
hi! link markdownH6 markdownH1
hi! link markdownIdDelimiter Keyword
hi! link markdownItalic Italic
hi! link markdownItalicDelimiter Keyword
hi! link markdownLinkDelimiter Keyword
hi! link markdownLinkTextDelimiter Keyword
hi! link markdownListMarker Keyword
hi! link markdownRule Keyword
hi! link markdownHeadingDelimiter Keyword

call s:hi("perlPackageDecl", s:paris7_gui, "", s:paris7_term, "", "", "")

call s:hi("phpClasses", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("phpDocTags", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link phpDocCustomTags phpDocTags
hi! link phpMemberSelector Keyword

call s:hi("podCmdText", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("podVerbatimLine", s:paris4_gui, "", "NONE", "", "", "")
hi! link podFormat Keyword

hi! link pythonBuiltin Type
hi! link pythonEscape SpecialChar

call s:hi("rubyConstant", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("rubySymbol", s:paris6_gui, "", s:paris6_term, "", s:bold, "")
hi! link rubyAttribute Identifier
hi! link rubyBlockParameterList Operator
hi! link rubyInterpolationDelimiter Keyword
hi! link rubyKeywordAsMethod Function
hi! link rubyLocalVariableOrMethod Function
hi! link rubyPseudoVariable Keyword
hi! link rubyRegexp SpecialChar

call s:hi("rustAttribute", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("rustEnum", s:paris7_gui, "", s:paris7_term, "", s:bold, "")
call s:hi("rustMacro", s:paris8_gui, "", s:paris8_term, "", s:bold, "")
call s:hi("rustModPath", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("rustPanic", s:paris9_gui, "", s:paris9_term, "", s:bold, "")
call s:hi("rustTrait", s:paris7_gui, "", s:paris7_term, "", s:italic, "")
hi! link rustCommentLineDoc Comment
hi! link rustDerive rustAttribute
hi! link rustEnumVariant rustEnum
hi! link rustEscape SpecialChar
hi! link rustQuestionMark Keyword

call s:hi("sassClass", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("sassId", s:paris7_gui, "", s:paris7_term, "", s:underline, "")
hi! link sassAmpersand Keyword
hi! link sassClassChar Delimiter
hi! link sassControl Keyword
hi! link sassControlLine Keyword
hi! link sassExtend Keyword
hi! link sassFor Keyword
hi! link sassFunctionDecl Keyword
hi! link sassFunctionName Function
hi! link sassidChar sassId
hi! link sassInclude SpecialChar
hi! link sassMixinName Function
hi! link sassMixing SpecialChar
hi! link sassReturn Keyword

hi! link shCmdParenRegion Delimiter
hi! link shCmdSubRegion Delimiter
hi! link shDerefSimple Identifier
hi! link shDerefVar Identifier

hi! link sqlKeyword Keyword
hi! link sqlSpecial Keyword

call s:hi("vimAugroup", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("vimMapRhs", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("vimNotation", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link vimFunc Function
hi! link vimFunction Function
hi! link vimUserFunc Function

call s:hi("xmlAttrib", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("xmlCdataStart", s:paris3_gui_bright, "", s:paris3_term, "", s:bold, "")
call s:hi("xmlNamespace", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link xmlAttribPunct Delimiter
hi! link xmlCdata Comment
hi! link xmlCdataCdata xmlCdataStart
hi! link xmlCdataEnd xmlCdataStart
hi! link xmlEndTag xmlTagName
hi! link xmlProcessingDelim Keyword
hi! link xmlTagName Keyword

call s:hi("yamlBlockMappingKey", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link yamlBool Keyword
hi! link yamlDocumentStart Keyword

"+----------------+
"+ Plugin Support +
"+----------------+
"+--- UI ---+
" ALE
" > w0rp/ale
call s:hi("ALEWarningSign", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("ALEErrorSign" , s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("ALEWarning" , s:paris13_gui, "", s:paris13_term, "", "undercurl", "")
call s:hi("ALEError" , s:paris11_gui, "", s:paris11_term, "", "undercurl", "")

" Coc
" > neoclide/coc.vim
call s:hi("CocWarningHighlight" , s:paris13_gui, "", s:paris13_term, "", "undercurl", "")
call s:hi("CocErrorHighlight" , s:paris11_gui, "", s:paris11_term, "", "undercurl", "")
call s:hi("CocWarningSign", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("CocErrorSign" , s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("CocInfoSign" , s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("CocHintSign" , s:paris10_gui, "", s:paris10_term, "", "", "")

if has('nvim')
  " Neovim LSP
  " > neovim/nvim-lspconfig
  call s:hi("LspCodeLens", s:paris3_gui_bright, "", s:paris3_term, "", "", "")
  if has("nvim-0.5")
    call s:hi("LspDiagnosticsDefaultWarning", s:paris13_gui, "", s:paris13_term, "", "", "")
    call s:hi("LspDiagnosticsDefaultError" , s:paris11_gui, "", s:paris11_term, "", "", "")
    call s:hi("LspDiagnosticsDefaultInformation" , s:paris8_gui, "", s:paris8_term, "", "", "")
    call s:hi("LspDiagnosticsDefaultHint" , s:paris10_gui, "", s:paris10_term, "", "", "")
    call s:hi("LspDiagnosticsUnderlineWarning" , s:paris13_gui, "", s:paris13_term, "", "undercurl", "")
    call s:hi("LspDiagnosticsUnderlineError" , s:paris11_gui, "", s:paris11_term, "", "undercurl", "")
    call s:hi("LspDiagnosticsUnderlineInformation" , s:paris8_gui, "", s:paris8_term, "", "undercurl", "")
    call s:hi("LspDiagnosticsUnderlineHint" , s:paris10_gui, "", s:paris10_term, "", "undercurl", "")
  endif
endif

" GitGutter
" > airblade/vim-gitgutter
call s:hi("GitGutterAdd", s:paris14_gui, "", s:paris14_term, "", "", "")
call s:hi("GitGutterChange", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("GitGutterChangeDelete", s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("GitGutterDelete", s:paris11_gui, "", s:paris11_term, "", "", "")

" Signify
" > mhinz/vim-signify
call s:hi("SignifySignAdd", s:paris14_gui, "", s:paris14_term, "", "", "")
call s:hi("SignifySignChange", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("SignifySignChangeDelete", s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("SignifySignDelete", s:paris11_gui, "", s:paris11_term, "", "", "")

" fugitive.vim
" > tpope/vim-fugitive
call s:hi("gitcommitDiscardedFile", s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("gitcommitUntrackedFile", s:paris11_gui, "", s:paris11_term, "", "", "")
call s:hi("gitcommitSelectedFile", s:paris14_gui, "", s:paris14_term, "", "", "")

" davidhalter/jedi-vim
call s:hi("jediFunction", s:paris4_gui, s:paris3_gui, "", s:paris3_term, "", "")
call s:hi("jediFat", s:paris8_gui, s:paris3_gui, s:paris8_term, s:paris3_term, s:underline.s:bold, "")

" NERDTree
" > scrooloose/nerdtree
call s:hi("NERDTreeExecFile", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link NERDTreeDirSlash Keyword
hi! link NERDTreeHelp Comment

" CtrlP
" > ctrlpvim/ctrlp.vim
hi! link CtrlPMatch Keyword
hi! link CtrlPBufferHid Normal

" vim-clap
" > liuchengxu/vim-clap
call s:hi("ClapDir", s:paris4_gui, "", "", "", "", "")
call s:hi("ClapDisplay", s:paris4_gui, s:paris1_gui, "", s:paris1_term, "", "")
call s:hi("ClapFile", s:paris4_gui, "", "", "NONE", "", "")
call s:hi("ClapMatches", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("ClapNoMatchesFound", s:paris13_gui, "", s:paris13_term, "", "", "")
call s:hi("ClapSelected", s:paris7_gui, "", s:paris7_term, "", s:bold, "")
call s:hi("ClapSelectedSign", s:paris9_gui, "", s:paris9_term, "", "", "")
let s:clap_matches = [
        \ [s:paris8_gui,  s:paris8_term] ,
        \ [s:paris9_gui,  s:paris9_term] ,
        \ [s:paris10_gui, s:paris10_term] ,
        \ ]
for s:paris_clap_match_i in range(1,12)
  let clap_match_color = s:clap_matches[s:paris_clap_match_i % len(s:clap_matches) - 1]
  call s:hi("ClapMatches" . s:paris_clap_match_i, clap_match_color[0], "", clap_match_color[1], "", "", "")
  call s:hi("ClapFuzzyMatches" . s:paris_clap_match_i, clap_match_color[0], "", clap_match_color[1], "", "", "")
endfor
unlet s:paris_clap_match_i
hi! link ClapCurrentSelection PmenuSel
hi! link ClapCurrentSelectionSign ClapSelectedSign
hi! link ClapInput Pmenu
hi! link ClapPreview Pmenu
hi! link ClapProviderAbout ClapDisplay
hi! link ClapProviderColon Type
hi! link ClapProviderId Type

" vim-indent-guides
" > nathanaelkane/vim-indent-guides
call s:hi("IndentGuidesEven", "", s:paris1_gui, "", s:paris1_term, "", "")
call s:hi("IndentGuidesOdd", "", s:paris2_gui, "", s:paris3_term, "", "")

" vim-plug
" > junegunn/vim-plug
call s:hi("plugDeleted", s:paris11_gui, "", "", s:paris11_term, "", "")

" vim-signature
" > kshenoy/vim-signature
call s:hi("SignatureMarkText", s:paris8_gui, "", s:paris8_term, "", "", "")

" vim-startify
" > mhinz/vim-startify
call s:hi("StartifyFile", s:paris6_gui, "", s:paris6_term, "", "", "")
call s:hi("StartifyFooter", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("StartifyHeader", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("StartifyNumber", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("StartifyPath", s:paris8_gui, "", s:paris8_term, "", "", "")
hi! link StartifyBracket Delimiter
hi! link StartifySlash Normal
hi! link StartifySpecial Comment

"+--- Languages ---+
" Haskell
" > neovimhaskell/haskell-vim
call s:hi("haskellPreProc", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("haskellType", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link haskellPragma haskellPreProc

" JavaScript
" > pangloss/vim-javascript
call s:hi("jsGlobalNodeObjects", s:paris8_gui, "", s:paris8_term, "", s:italic, "")
hi! link jsBrackets Delimiter
hi! link jsFuncCall Function
hi! link jsFuncParens Delimiter
hi! link jsThis Keyword
hi! link jsNoise Delimiter
hi! link jsPrototype Keyword
hi! link jsRegexpString SpecialChar

" Pandoc
" > vim-pandoc/vim-pandoc-syntax
call s:hi("pandocDefinitionBlockTerm", s:paris7_gui, "", s:paris7_term, "", s:italic, "")
call s:hi("pandocTableDelims", s:paris3_gui, "", s:paris3_term, "", "", "")
hi! link pandocAtxHeader markdownH1
hi! link pandocBlockQuote markdownBlockquote
hi! link pandocCiteAnchor Operator
hi! link pandocCiteKey pandocReferenceLabel
hi! link pandocDefinitionBlockMark Operator
hi! link pandocEmphasis markdownItalic
hi! link pandocFootnoteID pandocReferenceLabel
hi! link pandocFootnoteIDHead markdownLinkDelimiter
hi! link pandocFootnoteIDTail pandocFootnoteIDHead
hi! link pandocGridTableDelims pandocTableDelims
hi! link pandocGridTableHeader pandocTableDelims
hi! link pandocOperator Operator
hi! link pandocPipeTableDelims pandocTableDelims
hi! link pandocReferenceDefinition pandocReferenceLabel
hi! link pandocReferenceLabel markdownLinkText
hi! link pandocReferenceURL markdownUrl
hi! link pandocSimpleTableHeader pandocAtxHeader
hi! link pandocStrong markdownBold
hi! link pandocTableHeaderWord pandocAtxHeader
hi! link pandocUListItemBullet Operator
  
if has('nvim')
  " tree-sitter
  " > nvim-treesitter/nvim-treesitter
  hi! link TSAnnotation Annotation
  hi! link TSConstBuiltin Constant
  hi! link TSConstructor Function
  hi! link TSEmphasis Italic
  hi! link TSFuncBuiltin Function
  hi! link TSFuncMacro Function
  hi! link TSStringRegex SpecialChar
  hi! link TSStrong Bold
  hi! link TSStructure Structure
  hi! link TSTagDelimiter TSTag
  hi! link TSUnderline Underline
  hi! link TSVariable Variable
  hi! link TSVariableBuiltin Keyword
endif

" TypeScript
" > HerringtonDarkholme/yats.vim
call s:hi("typescriptBOMWindowMethod", s:paris8_gui, "", s:paris8_term, "", s:italic, "")
call s:hi("typescriptClassName", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("typescriptDecorator", s:paris12_gui, "", s:paris12_term, "", "", "")
call s:hi("typescriptInterfaceName", s:paris7_gui, "", s:paris7_term, "", s:bold, "")
call s:hi("typescriptRegexpString", s:paris13_gui, "", s:paris13_term, "", "", "")
" TypeScript JSX
 call s:hi("tsxAttrib", s:paris7_gui, "", s:paris7_term, "", "", "")
hi! link typescriptOperator Operator
hi! link typescriptBinaryOp Operator
hi! link typescriptAssign Operator
hi! link typescriptMember Identifier
hi! link typescriptDOMStorageMethod Identifier
hi! link typescriptArrowFuncArg Identifier
hi! link typescriptGlobal typescriptClassName
hi! link typescriptBOMWindowProp Function
hi! link typescriptArrowFuncDef Function
hi! link typescriptAliasDeclaration Function
hi! link typescriptPredefinedType Type
hi! link typescriptTypeReference typescriptClassName
hi! link typescriptTypeAnnotation Structure
hi! link typescriptDocNamedParamType SpecialComment
hi! link typescriptDocNotation Keyword
hi! link typescriptDocTags Keyword
hi! link typescriptImport Keyword
hi! link typescriptExport Keyword
hi! link typescriptTry Keyword
hi! link typescriptVariable Keyword
hi! link typescriptBraces Normal
hi! link typescriptObjectLabel Normal
hi! link typescriptCall Normal
hi! link typescriptClassHeritage typescriptClassName
hi! link typescriptFuncTypeArrow Structure
hi! link typescriptMemberOptionality Structure
hi! link typescriptNodeGlobal typescriptGlobal
hi! link typescriptTypeBrackets Structure
hi! link tsxEqual Operator
hi! link tsxIntrinsicTagName htmlTag
hi! link tsxTagName tsxIntrinsicTagName

" Markdown
" > plasticboy/vim-markdown
call s:hi("mkdCode", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("mkdFootnote", s:paris8_gui, "", s:paris8_term, "", "", "")
call s:hi("mkdRule", s:paris10_gui, "", s:paris10_term, "", "", "")
call s:hi("mkdLineBreak", s:paris9_gui, "", s:paris9_term, "", "", "")
hi! link mkdBold Bold
hi! link mkdItalic Italic
hi! link mkdString Keyword
hi! link mkdCodeStart mkdCode
hi! link mkdCodeEnd mkdCode
hi! link mkdBlockquote Comment
hi! link mkdListItem Keyword
hi! link mkdListItemLine Normal
hi! link mkdFootnotes mkdFootnote
hi! link mkdLink markdownLinkText
hi! link mkdURL markdownUrl
hi! link mkdInlineURL mkdURL
hi! link mkdID Identifier
hi! link mkdLinkDef mkdLink
hi! link mkdLinkDefTarget mkdURL
hi! link mkdLinkTitle mkdInlineURL
hi! link mkdDelimiter Keyword

" PHP
" > StanAngeloff/php.vim
call s:hi("phpClass", s:paris7_gui, "", s:paris7_term, "", "", "")
call s:hi("phpClassImplements", s:paris7_gui, "", s:paris7_term, "", s:bold, "")
hi! link phpClassExtends phpClass
hi! link phpFunction Function
hi! link phpMethod Function
hi! link phpUseClass phpClass

" Vimwiki
" > vimwiki/vimwiki
if !exists("g:vimwiki_hl_headers") || g:vimwiki_hl_headers == 0
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:paris8_gui, "", s:paris8_term, "", s:bold, "")
  endfor
else
  let s:vimwiki_hcolor_guifg = [s:paris7_gui, s:paris8_gui, s:paris9_gui, s:paris10_gui, s:paris14_gui, s:paris15_gui]
  let s:vimwiki_hcolor_ctermfg = [s:paris7_term, s:paris8_term, s:paris9_term, s:paris10_term, s:paris14_term, s:paris15_term]
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:vimwiki_hcolor_guifg[s:i-1] , "", s:vimwiki_hcolor_ctermfg[s:i-1], "", s:bold, "")
  endfor
endif
call s:hi("VimwikiLink", s:paris8_gui, "", s:paris8_term, "", s:underline, "")
hi! link VimwikiHeaderChar markdownHeadingDelimiter
hi! link VimwikiHR Keyword
hi! link VimwikiList markdownListMarker

" YAML
" > stephpy/vim-yaml
call s:hi("yamlKey", s:paris7_gui, "", s:paris7_term, "", "", "")

"+------------+
"+ Public API +
"+------------+
"+--- Functions ---+

function! ParisPalette() abort
  let ret = {}
  for color in range(16)
    execute 'let ret["paris'.color.'"] = s:paris'.color.'_gui'
  endfor
  let ret["paris3_bright"] = s:paris3_gui_bright
  return ret
endfunction
