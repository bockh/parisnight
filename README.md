# Paris Night Color Palette

## Inspiration

Paris Night is an attempt to recreate the feeling of sitting on my balcony in the 17eme.

![picture](photo/green.jpg)

The original color diagram is created using HTML and CSS.

[Inspiration Trends](https://www.architecturaldigest.com/gallery/the-8-colors-that-were-everywhere-in-paris)
- Rust
- Moss
- Pale Purple
- Straw
- Burgundy
- Citrus Orange
- Cream
- Gray
- Acid Green
- Tortoise Brown

##  Colors

This scheme consists of 9 colors with two varieties: muted and bright.
The first colors are standard foreground and background.
The remaining colors are modifications for emphasis on certain tokens.

<!-- |  |  | 40m | 41m | 42m | 43m | 44m | 45m | 46m | 47m | -->
<!-- |--|--|-----|-----|-----|-----|-----|-----|-----|-----| -->
<!-- |m| -->
<!-- |1m| -->
<!-- |30m| -->
<!-- |1;30m| -->
<!-- |31m| -->
<!-- |32m| -->
<!-- |1;32m| -->
<!-- |33m| -->
<!-- |1;33m| -->
<!-- |34m| -->
<!-- |1;34m| -->
<!-- |35m| -->
<!-- |1;35m| -->
<!-- |36m| -->
<!-- |1;36m| -->
<!-- |37m| -->
<!-- |1;37m| -->

## La Nuit

La Nuit is an intermediary scheme for Neovim GUIs.
